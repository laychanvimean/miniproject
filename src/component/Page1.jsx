import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
export default class Page1 extends Component {
    constructor(props) {
		super(props);
		this.state = {
			job: []
		}
		this.handleChange = this.handleChange.bind(this);
	}
	handleChange(e) {
		const { checked, value } = e.target;
		let { job } = this.state;
		if(checked && job.indexOf(value) === -1) {
			job.push(value);
		}else {
			job = job.filter(i => i !== value)
		}
		this.setState({
			job
		});
	}
	render () {
		const { job } = this.state;
		return (
			<div class='container'>
                <p style={{fontSize:'30px'}}>Job:</p>
		
				<div class='checkbox'>
				<label>
					<input
						type='checkbox'
						value='Students'
						checked={job.indexOf('Students') !== -1}
						onChange={this.handleChange} />	Students
				</label>
                &nbsp;&nbsp;&nbsp;
			   <label>
					<input
						type='checkbox'
						value='Teacher'
						checked={job.indexOf('Teacher') !== -1}
						onChange={this.handleChange} />Teacher
				</label>
                &nbsp;&nbsp;&nbsp;
                <label>
					<input
						type='checkbox'
						value='Developer'
						checked={job.indexOf('Developer') !== -1}
						onChange={this.handleChange} />Developer
				</label>
				</div>
				   <br />
                  <td>
                    <ul>
                        {job.map((job) => (<li key={job}>{job}</li>))}
                    </ul>
                  </td>
				
			</div>
		)
	}
}
    
        
    

