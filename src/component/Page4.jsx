import React, { Component } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css';
export default class Page4 extends Component {
    constructor(props) {
		super(props);
		this.state = {
			Gender: []
		}
		this.handleChange = this.handleChange.bind(this);
	}
	handleChange(e) {
		const { checked, value } = e.target;
		let { Gender } = this.state;
		if(checked && Gender.indexOf(value) === -1) {
			Gender.push(value);
		}else {
			Gender = Gender.filter(i => i !== value)
		}
		this.setState({
			Gender
		});
	}
	render () {
		const { Gender } = this.state;
		return (
			<div class='container'>
                <p style={{fontSize:'30px'}}>Gender:</p>
		
				<div class='checkbox'>
				<label>
					<input
						type='checkbox'
						value='Female'
						checked={Gender.indexOf('Female') !== -1}
						onChange={this.handleChange} />	Female
				</label>
                &nbsp;&nbsp;&nbsp;
			   <label>
					<input
						type='checkbox'
						value='Male'
						checked={Gender.indexOf('Male') !== -1}
						onChange={this.handleChange} />Male
				</label>
          
				</div>
				   <br />
                  <td>
                    <ul>
                        {Gender.map((Gender) => (<li key={Gender}>{Gender}</li>))}
                    </ul>
                  </td>
				
			</div>
		)
	}
}
    
   
