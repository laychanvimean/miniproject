
import React, { Component } from "react";
 
class Item extends Component {
  state = {
    count: this.props.value
  };
 
  render() {
    return (
      <React.Fragment>
        <div className="card mb-2">
          <div className="card-body">
            <button
              onClick={() => this.props.onDelete(this.props.id)}
              className="btn btn-lg btn-outline-danger ml-4">
              Delete
            </button>
          </div>
        </div>
      </React.Fragment>
    );
  }
 

}
 
export default Item;